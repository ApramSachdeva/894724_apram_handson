package com.restapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restapi.exceptions.ContryNotFoundException;
import com.restapi.models.Country;
import com.restapi.services.CountryService;

@RestController
public class HomeController {
	
	@Autowired
	private CountryService countryService;
	
	@GetMapping("/hello")
	public String sayHello()
	{
		return "Hello World!!";
	}
	@RequestMapping(value="/country",method = RequestMethod.GET)
	 public  Country  getCountryIndia()
	{
		
		
		return countryService.getCountry();
	}
	
	
	@GetMapping("/countries/{code}")
public Country	getCountry(@PathVariable("code") String code) throws ContryNotFoundException
	{
		return countryService.getCountry(code);
	}
	
@GetMapping("/countries")
	
	public List<Country> getAllCountries()
	{
	  
	     
	
		return countryService.getContries();
	}
}
