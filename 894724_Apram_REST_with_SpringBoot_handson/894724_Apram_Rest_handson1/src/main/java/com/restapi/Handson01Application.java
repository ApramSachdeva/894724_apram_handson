package com.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource(value = "classpath:country.xml")
public class Handson01Application {

	public static void main(String[] args) {
		SpringApplication.run(Handson01Application.class, args);
	}

}
