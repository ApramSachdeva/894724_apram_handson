package com.restapi.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND,reason = "Country Not Found")
public class ContryNotFoundException extends Exception {

	public ContryNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
	
	

}
