package com.restapi.exceptions;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.restapi.models.ErrorDetails;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler(ContryNotFoundException.class)
	public ResponseEntity<ErrorDetails> handleCountryNotFoundExEntity(ContryNotFoundException ex,WebRequest req)
	{
		ErrorDetails error = new ErrorDetails();
		error.setStatus(HttpStatus.NOT_FOUND.value());
        error.setError(HttpStatus.NOT_FOUND.getReasonPhrase());
        error.setTimestamp(new Date());
        error.setMessage(ex.getMessage());
        error.setPath(req.getDescription(false));

   return new ResponseEntity<ErrorDetails>(error, HttpStatus.NOT_FOUND);
	}

}
