package com.restapi.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.restapi.exceptions.ContryNotFoundException;
import com.restapi.models.Country;

@Service
public class CountryService {
	@Autowired
	 private ApplicationContext context;
	
	
	public Country getCountry()
	{
		Country c = (Country)context.getBean("india");
		return c;
	}
	public Country getCountry(String code) throws ContryNotFoundException
	{
		 Map<String, Country>  countriesMap =   context.getBeansOfType(Country.class);
			
		  
	     List<Country> countryList = new ArrayList<Country>();
	     
	     
	     for(Map.Entry<String, Country> cn :countriesMap.entrySet())
	     {
	    	 countryList.add(cn.getValue());
	    	 
	     }
		
	     Country country = null;
	     for(Country c :countryList)
	     {
	    	 if(c.getCode().equalsIgnoreCase(code))
	    	 {
	    		country = c;
	    	 }
	     }
	     
		
	     if(country!=null)
	     {
	    	 return country;
	     }
	     else
	     {
	    	 throw new ContryNotFoundException("Country Not Found");
	     }
		
		
	}
	
	public List<Country> getContries()
	{
		 Map<String, Country>  countriesMap =   context.getBeansOfType(Country.class);
			
		  
	     List<Country> countryList = new ArrayList<Country>();
	     
	     
	     for(Map.Entry<String, Country> cn :countriesMap.entrySet())
	     {
	    	 countryList.add(cn.getValue());
	    	 
	     }
	     
	     
	
		return countryList;
	}
}
